<?php


Route::get('home', 'HomeController@index')->name('home.index');


Route::resource('estados', 'EstadoController'); //admin.estados.index


Route::resource('areas', 'AreaController');


Route::resource('tipos_tramite', 'TipoTramiteController');

Route::resource('tramites', 'TramiteController');

Route::resource('cargos', 'CargoController');