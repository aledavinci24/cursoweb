<?php

namespace App\Http\Controllers\Admin;

use App\Estado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstadoController extends Controller
{

    public function index()
    {
        $estados = Estado::orderBy('estado')->paginate(5);

        return view('admin.estados.index', compact('estados'));
    }

    public function create()
    {
        return view('admin.estados.create');
    }

    public function store(Request $request)
    {
        Estado::create([
            'estado' => $request->input('estado'),
        ]);

        return redirect(route('admin.estados.index'))->with('success', 'Estado Creado correctamente');
    }

    public function show($id)
    {
        $estado = Estado::where('id', $id)->first();

        return view('admin.estados.delete', compact('estado'));
    }

    public function edit($id)
    {
        $estado = Estado::where('id', $id)->first();

        return view('admin.estados.edit', compact('estado'));
    }


    public function update(Request $request, $id)
    {
        Estado::where('id', $id)->update([
            'estado' => $request->input('estado')
        ]);

        return redirect(route('admin.estados.index'))->with('success', 'Estado actualizado correctamente');
    }

    public function destroy($id)
    {
        Estado::where('id', $id)->delete();

        return redirect(route('admin.estados.index'))->with('success', 'Estado eliminado correctamente');
    }
}
