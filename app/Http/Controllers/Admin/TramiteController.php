<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TramiteController extends Controller
{
    public function index()
    {
        $actividades = Actividad::orderBy('nombre')->get();

        return view('admin.actividad.index', compact('actividades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.actividad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Actividad::create([
            'nombre'=> $request->input('nombre'),
            'cupo_maximo'=> $request->input('cupo_maximo')
        ]);

        return redirect(route('admin.actividades.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actividad = Actividad::where('id', $id)->first();

        return view('admin.actividad.delete', compact('actividad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividad = Actividad::where('id', $id)->first();

        return view('admin.actividad.edit', compact('actividad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Actividad::where('id', $id)->update([
            'nombre' => $request->input('nombre'),
            'cupo_maximo' => $request->input('cupo_maximo')
        ]);

        return redirect(route('admin.actividades.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Actividad::where('id', $id)->delete();

        return redirect(route('admin.actividades.index'));
    }
}
