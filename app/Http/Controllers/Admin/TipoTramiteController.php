<?php

namespace App\Http\Controllers\Admin;

use App\TipoTramite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoTramiteController extends Controller
{
    public function index()
    {
        $tipos_tramite = TipoTramite::orderBy('tipo_tramite')->get();

        return view('admin.tipos_tramite.index', compact('tipos_tramite'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tipos_tramite.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        TipoTramite::create([
            'tipo_tramite'=> $request->input('tipo_tramite')
        ]);

        return redirect(route('admin.tipos_tramite.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipo_tramite = TipoTramite::where('id', $id)->first();

        return view('admin.tipos_tramite.delete', compact('tipo_tramite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo_tramite = TipoTramite::where('id', $id)->first();

        return view('admin.tipos_tramite.edit', compact('tipo_tramite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        TipoTramite::where('id', $id)->update([
            'tipo_tramite' => $request->input('tipo_tramite')
        ]);

        return redirect(route('admin.tipos_tramite.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TipoTramite::where('id', $id)->delete();

        return redirect(route('admin.tipos_tramite.index'));
    }
}
