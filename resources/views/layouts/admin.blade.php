<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('libs/bootstrap4/bootstrap.css')}}">

    <link rel="stylesheet" href="{{asset('libs/datatables/datatables.min.css')}}">

    <link rel="stylesheet" href="{{asset('libs/fontawesome/all.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{{asset('libs/adminlte/adminlte.css')}}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="{{{asset('/css/app.css')}}}">

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{route('admin.home.index')}}" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contact</a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @auth
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @if(Auth::user()->role == 'admin')
                            <a class="dropdown-item" href="{{ route('home') }}">
                                Modo usuario
                            </a>
                            <hr>
                        @endif
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endauth
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                            class="fa fa-th-large"></i></a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link">
            <img src="{{asset('img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{asset('img/user1-128x128.jpg')}}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{Auth::user()->name}} {{Auth::user()->lastname}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{route('admin.home.index')}}" class="nav-link {{ request()->is('admin/home') ? 'active' : '' }} ">
                            <i class="nav-icon fa fa-tachometer-alt fa-fw"></i>
                            <p>
                                Home
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.users.index')}}" class="nav-link {{ request()->is('users/*') ? 'active' : ''}}">
                            <i class="nav-icon fa fa-users fa-fw"></i>
                            <p>
                                Usuarios

                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.areas.index')}}" class="nav-link">
                            <i class="fa fa-map-marker fa-fw nav-icon" aria-hidden="true"></i>
                            <p>
                                Areas
                            </p>
                        </a>
                    </li>

                    <li class="nav-item ">
                        <a href="{{route('admin.estados.index')}}" class="nav-link {{ request()->is('admin/estados') ? 'active' : '' }}">
                            <i class="nav-icon fa fa-dot-circle fa-fw"></i>
                            <p>
                                Estados
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.cargos.index')}}" class="nav-link">
                            <i class="fa fa-address-card nav-icon" aria-hidden="true"></i>
                            <p>
                                Cargos
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-file fa-fw"></i>
                            <p>
                                Tramites
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.tipos_tramite.index')}}" class="nav-link">
                                    <i class="fa fa-fw fa-tachometer-alt nav-icon"></i>
                                    <p>Tipos</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.tramites.index')}}" class="nav-link">
                                    <i class="fa fa-fw fa-file nav-icon"></i>
                                    <p>Trámites</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            @yield('title')
                        </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('admin.home.index')}}">Home</a></li>
                            @yield('link')
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @yield('content')
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('/libs/jquery/jquery-3.3.1.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('/libs/bootstrap4/bootstrap.bundle.js')}}"></script>

<script src="{{asset('/libs/datatables/datatables.min.js')}}"></script>
<script src="{{asset('/libs/sweetalert/sweetalert2.all.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('/libs/adminlte/adminlte.js')}}"></script>

<script src="{{asset('/js/app.js')}}"></script>

@include('sweetalert::alert')

</body>




</html>
