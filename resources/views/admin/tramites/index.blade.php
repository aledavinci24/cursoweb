@extends('admin.layout')

@section('title')

    Actividades

    <a href="{{route('admin.actividades.create')}}" class="btn btn-info btn-sm">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </a>

@endsection

@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table-bordered table">
                <thead>
                <tr>
                    <td class="text-center">Actividad</td>
                    <td class="text-center">Cupo Máximo</td>
                    <td class="text-center">Acciones</td>
                </tr>
                </thead>
                <tbody>
                @foreach($actividades as $a)
                    <tr class="text-center">
                        <th>
                            {{$a->nombre}}
                        </th>
                        <th>
                            {{$a->cupo_maximo}}
                        </th>
                        <th>
                            <div class="btn-group">
                                <i class="fa fa-list" aria-hidden="true" data-toggle="dropdown"
                                   aria-expanded="false" style="cursor: pointer; color: #1d68a7">
                                </i>

                                <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 38px, 0px);">
                                    <a class="dropdown-item" href="{{route('admin.actividades.edit', $a->id)}}">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        Editar
                                    </a>
                                    <a class="dropdown-item" href="{{route('admin.actividades.show', $a->id)}}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        Eliminar
                                    </a>
                                </div>
                            </div>
                        </th>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>


@endsection
