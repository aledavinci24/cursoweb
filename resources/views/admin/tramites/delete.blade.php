@extends('admin.layout')

@section('title')

    Eliminar Actividad


@endsection

@section('content')


    <div class="card card-info">

        <div class="card-body">

            <!-- form start -->
            <form method="POST" action="{{route('admin.actividades.destroy', $actividad->id)}}" class="form-horizontal">
                @csrf
                @method('delete')

                <div class="alert alert-danger text-center" style="overflow: hidden;">

                    ¿ Eliminar la actividad <strong>{{$actividad->nombre}}</strong>?

                </div>

                <a href="{{route('admin.actividades.index')}}" class="btn btn-default float-left">Cancel</a>

                <input type="submit" class="btn btn-danger float-right" value="Eliminar"/>

                <!-- /.card-footer -->
            </form>

        </div>
    </div>

@endsection
