@extends('admin.layout')

@section('title')

    Editar Actividad


@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.actividades.update', $actividad->id)}}" class="form-horizontal">
            @csrf
            @method('patch')

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Actividad</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="nombre" value="{{ $actividad->nombre }}" placeholder="Ej: Complemento..." autofocus required autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Cupo máximo</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="cupo_maximo" placeholder="Ej: 10..." required autocomplete="off" value="{{  $actividad->cupo_maximo }}">
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-warning float-right"  value="Editar"/>
                <a href="{{route('admin.actividades.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>


@endsection
