@extends('layouts.admin')

@section('title')

    Editar Tipo de Trámite


@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.tipos_tramite.update', $tipo_tramite->id)}}" class="form-horizontal">
            @csrf
            @method('patch')

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tipo de Trámite</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="tipo_tramite" value="{{ $tipo_tramite->tipo_tramite }}" autofocus required autocomplete="off">
                    </div>
                </div>
               

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-warning float-right"  value="Editar"/>
                <a href="{{route('admin.tipos_tramite.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>


@endsection
