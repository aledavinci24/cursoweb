@extends('layouts.admin')


@section('title')

    Nueva Cargo

@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.cargos.store')}}" class="form-horizontal">
            @csrf

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Cargo</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="cargo" value="{{old('cargo')}}" placeholder="-" autofocus required autocomplete="off">
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-info float-right"  value="Crear"/>
                <a href="{{route('admin.cargos.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>

@endsection
