@extends('layouts.admin')

@section('title')
    Usuarios
@endsection

@section('link')
    <li class="breadcrumb-item active">
        <a href="{{route('admin.users.index')}}">Usuarios</a>
    </li>
@endsection

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">Lista de usuarios</h5>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th class="text-center">
                            <a class="btn btn-primary btn-sm" href="#">Nuevo usuario</a>
                        </th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{{$user->id}}}</td>
                            <td>{{{$user->name}}}</td>
                            <td>{{{$user->lastname}}}</td>
                            <td>{{{$user->email}}}</td>
                            <td>{{{$user->role}}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a class="btn btn-success btn-sm" href="#">Editar</a>
                                    <a class="btn btn-danger btn-sm" href="{{url('/user/destroy/' . $user->id)}}">Eliminar</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
