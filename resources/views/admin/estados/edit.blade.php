@extends('layouts.admin')

@section('title')

    Editar Estado


@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.estados.update', $estado->id)}}" class="form-horizontal">
            @csrf
            @method('patch')

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Estado</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="estado" value="{{ $estado->estado }}" placeholder="..." autofocus required autocomplete="off">
                    </div>
                </div>
               

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-warning float-right"  value="Editar"/>
                <a href="{{route('admin.estados.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>


@endsection
