@extends('layouts.admin')

@section('title')

    Editar Área


@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.areas.update', $area->id)}}" class="form-horizontal">
            @csrf
            @method('patch')

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Área</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="area" value="{{ $area->area }}" placeholder="..." autofocus required autocomplete="off">
                    </div>
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-warning float-right"  value="Editar"/>
                <a href="{{route('admin.areas.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>


@endsection
