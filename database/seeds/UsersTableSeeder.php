<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Guillermo',
            'lastname' => 'Colotti',
            'email' => 'admin@admin.com',
            'role' => 'admin',
            'password' => bcrypt('admin')
        ]);

        DB::table('users')->insert([
            'name' => 'Pepe',
            'lastname' => 'Martinez',
            'email' => 'user@user.com',
            'role' => 'user',
            'password' => bcrypt('user')
        ]);


//        factory(\App\User::class, 10)->create();
    }
}
